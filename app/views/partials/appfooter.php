<footer class="footer border-top">
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-4">
				<div class="copyright"> Komplek Ruko Plaza Kedoya Elok Blok DE No.19-20, Jl. Panjang, Kebon Jeruk, Kedoya Selatan, RT.19/RW.4, Kedoya Sel., Kec. Kb. Jeruk, Kota Jakarta Barat - (021) 58300098 |  <?php echo date('Y') ?></div>
			</div>
			<div class="col">
				<div class="footer-links text-right">
					<a href="<?php print_link('info/about'); ?>">About us</a> | 
					<a href="<?php print_link('info/help'); ?>">Help and FAQ</a> |
					<a href="<?php print_link('info/contact'); ?>">Contact us</a>  |
					<a href="<?php print_link('info/privacy_policy'); ?>">Privacy Policy</a> |
					<a href="<?php print_link('info/terms_and_conditions'); ?>">Terms and Conditions</a>
				</div>
			</div>
			
		</div>
	</div>
</footer>