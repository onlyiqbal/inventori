<?php 

/**
 * SharedController Controller
 * @category  Controller / Model
 */
class SharedController extends BaseController{
	
	/**
     * barang_keluar_kode_barang_option_list Model Action
     * @return array
     */
	function barang_keluar_kode_barang_option_list(){
		$db = $this->GetModel();
		$sqltext = "SELECT  DISTINCT kode_barang AS value,nama_barang AS label FROM data_barang";
		$queryparams = null;
		$arr = $db->rawQuery($sqltext, $queryparams);
		return $arr;
	}

	/**
     * barang_keluar_penerima_option_list Model Action
     * @return array
     */
	function barang_keluar_penerima_option_list(){
		$db = $this->GetModel();
		$sqltext = "SELECT DISTINCT id AS value , username AS label FROM user ORDER BY label ASC";
		$queryparams = null;
		$arr = $db->rawQuery($sqltext, $queryparams);
		return $arr;
	}

	/**
     * barang_masuk_kode_barang_option_list Model Action
     * @return array
     */
	function barang_masuk_kode_barang_option_list(){
		$db = $this->GetModel();
		$sqltext = "SELECT  DISTINCT kode_barang AS value,nama_barang AS label FROM data_barang";
		$queryparams = null;
		$arr = $db->rawQuery($sqltext, $queryparams);
		return $arr;
	}

	/**
     * barang_masuk_penerima_option_list Model Action
     * @return array
     */
	function barang_masuk_penerima_option_list(){
		$db = $this->GetModel();
		$sqltext = "SELECT DISTINCT id AS value , username AS label FROM user ORDER BY label ASC";
		$queryparams = null;
		$arr = $db->rawQuery($sqltext, $queryparams);
		return $arr;
	}

	/**
     * user_username_value_exist Model Action
     * @return array
     */
	function user_username_value_exist($val){
		$db = $this->GetModel();
		$db->where("username", $val);
		$exist = $db->has("user");
		return $exist;
	}

	/**
     * user_email_value_exist Model Action
     * @return array
     */
	function user_email_value_exist($val){
		$db = $this->GetModel();
		$db->where("email", $val);
		$exist = $db->has("user");
		return $exist;
	}

	/**
     * user_user_role_id_option_list Model Action
     * @return array
     */
	function user_user_role_id_option_list(){
		$db = $this->GetModel();
		$sqltext = "SELECT role_id AS value, role_name AS label FROM roles";
		$queryparams = null;
		$arr = $db->rawQuery($sqltext, $queryparams);
		return $arr;
	}

	/**
     * getcount_databarang Model Action
     * @return Value
     */
	function getcount_databarang(){
		$db = $this->GetModel();
		$sqltext = "SELECT COUNT(*) AS num FROM data_barang";
		$queryparams = null;
		$val = $db->rawQueryValue($sqltext, $queryparams);
		
		if(is_array($val)){
			return $val[0];
		}
		return $val;
	}

	/**
     * getcount_barangmasuk Model Action
     * @return Value
     */
	function getcount_barangmasuk(){
		$db = $this->GetModel();
		$sqltext = "SELECT COUNT(*) AS num FROM barang_masuk";
		$queryparams = null;
		$val = $db->rawQueryValue($sqltext, $queryparams);
		
		if(is_array($val)){
			return $val[0];
		}
		return $val;
	}

	/**
     * getcount_barangkeluar Model Action
     * @return Value
     */
	function getcount_barangkeluar(){
		$db = $this->GetModel();
		$sqltext = "SELECT COUNT(*) AS num FROM barang_keluar";
		$queryparams = null;
		$val = $db->rawQueryValue($sqltext, $queryparams);
		
		if(is_array($val)){
			return $val[0];
		}
		return $val;
	}

	/**
     * getcount_user Model Action
     * @return Value
     */
	function getcount_user(){
		$db = $this->GetModel();
		$sqltext = "SELECT COUNT(*) AS num FROM user";
		$queryparams = null;
		$val = $db->rawQueryValue($sqltext, $queryparams);
		
		if(is_array($val)){
			return $val[0];
		}
		return $val;
	}

}
