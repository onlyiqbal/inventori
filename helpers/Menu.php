<?php
/**
 * Menu Items
 * All Project Menu
 * @category  Menu List
 */

class Menu{
	
	
			public static $navbarsideleft = array(
		array(
			'path' => 'home', 
			'label' => 'Home', 
			'icon' => '<i class="material-icons ">home</i>'
		),
		
		array(
			'path' => 'data_barang', 
			'label' => 'Data Barang', 
			'icon' => '<i class="material-icons ">shopping_cart</i>'
		),
		
		array(
			'path' => 'barang_masuk', 
			'label' => 'Barang Masuk', 
			'icon' => '<i class="material-icons ">arrow_back</i>'
		),
		
		array(
			'path' => 'barang_keluar', 
			'label' => 'Barang Keluar', 
			'icon' => '<i class="material-icons ">arrow_forward</i>'
		),
		
		array(
			'path' => 'user', 
			'label' => 'User', 
			'icon' => '<i class="material-icons ">person</i>'
		),
		
		array(
			'path' => 'roles', 
			'label' => 'Roles', 
			'icon' => '<i class="material-icons ">build</i>'
		),
		
		array(
			'path' => 'role_permissions', 
			'label' => 'Role Permissions', 
			'icon' => '<i class="material-icons ">compare_arrows</i>'
		)
	);
		
	
	
			public static $nama_barang = array(
		array(
			"value" => "AGP Card", 
			"label" => "AGP Card", 
		),
		array(
			"value" => "AGP Card (Accelerated Graphics Port)", 
			"label" => "AGP Card (Accelerated Graphics Port)", 
		),
		array(
			"value" => "Barcode Reader", 
			"label" => "Barcode Reader", 
		),
		array(
			"value" => "Bride", 
			"label" => "Bride", 
		),
		array(
			"value" => "Card Reader", 
			"label" => "Card Reader", 
		),
		array(
			"value" => "CD", 
			"label" => "CD", 
		),
		array(
			"value" => "CD ROM / DVD ROM", 
			"label" => "CD ROM / DVD ROM", 
		),
		array(
			"value" => "CPU", 
			"label" => "CPU", 
		),
		array(
			"value" => "FDD – Floppy Disk Drive", 
			"label" => "FDD – Floppy Disk Drive", 
		),
		array(
			"value" => "Flashdisk", 
			"label" => "Flashdisk", 
		),
		array(
			"value" => "Hard Disk", 
			"label" => "Hard Disk", 
		),
		array(
			"value" => "Hard Disk Eksternal", 
			"label" => "Hard Disk Eksternal", 
		),
		array(
			"value" => "Headphone", 
			"label" => "Headphone", 
		),
		array(
			"value" => "Heatsink", 
			"label" => "Heatsink", 
		),
		array(
			"value" => "Hub / Switch", 
			"label" => "Hub / Switch", 
		),
		array(
			"value" => "Kabel Data", 
			"label" => "Kabel Data", 
		),
		array(
			"value" => "Keyboard", 
			"label" => "Keyboard", 
		),
		array(
			"value" => "LAN Card", 
			"label" => "Lan Card", 
		),
		array(
			"value" => "Magnetic Tape", 
			"label" => "Magnetic Tape", 
		),
		array(
			"value" => "Modem", 
			"label" => "Modem", 
		),
		array(
			"value" => "Monitor", 
			"label" => "Monitor", 
		),
		array(
			"value" => "Motherboard", 
			"label" => "Motherboard", 
		),
		array(
			"value" => "Mouse", 
			"label" => "Mouse", 
		),
		array(
			"value" => "Network Card", 
			"label" => "Network Card", 
		),
		array(
			"value" => "Optical Drive", 
			"label" => "Optical Drive", 
		),
		array(
			"value" => "Power Supply", 
			"label" => "Power Supply", 
		),
		array(
			"value" => "Printer", 
			"label" => "Printer", 
		),
		array(
			"value" => "Processor", 
			"label" => "Processor", 
		),
		array(
			"value" => "Projector", 
			"label" => "Projector", 
		),
		array(
			"value" => "RAM", 
			"label" => "Ram", 
		),
		array(
			"value" => "Repeater", 
			"label" => "Repeater", 
		),
		array(
			"value" => "RJ45", 
			"label" => "RJ45", 
		),
		array(
			"value" => "ROM", 
			"label" => "ROM", 
		),
		array(
			"value" => "Router", 
			"label" => "Router", 
		),
		array(
			"value" => "Scanner", 
			"label" => "Scanner", 
		),
		array(
			"value" => "Sound Card", 
			"label" => "Sound Card", 
		),
		array(
			"value" => "Speaker", 
			"label" => "Speaker", 
		),
		array(
			"value" => "SSD", 
			"label" => "SSD", 
		),
		array(
			"value" => "Touchpad", 
			"label" => "Touchpad", 
		),
		array(
			"value" => "TV Tuner Card", 
			"label" => "TV Tuner Card", 
		),
		array(
			"value" => "UPS – Uninterruptible Power Supply", 
			"label" => "Ups – Uninterruptible Power Supply", 
		),
		array(
			"value" => "VGA Card", 
			"label" => "VGA Card", 
		),
		array(
			"value" => "Web Cam", 
			"label" => "Web Cam", 
		),);
		
			public static $kondisi = array(
		array(
			"value" => "Baru", 
			"label" => "Baru", 
		),
		array(
			"value" => "Bekas", 
			"label" => "Bekas", 
		),
		array(
			"value" => "Rusak", 
			"label" => "Rusak", 
		),);
		
			public static $lokasi_lemari = array(
		array(
			"value" => "No.1", 
			"label" => "No.1", 
		),
		array(
			"value" => "No.2", 
			"label" => "No.2", 
		),
		array(
			"value" => "No.3", 
			"label" => "No.3", 
		),
		array(
			"value" => "No.4", 
			"label" => "No.4", 
		),
		array(
			"value" => "No.5", 
			"label" => "No.5", 
		),
		array(
			"value" => "No.6", 
			"label" => "No.6", 
		),
		array(
			"value" => "No.7", 
			"label" => "No.7", 
		),
		array(
			"value" => "No.8", 
			"label" => "No.8", 
		),
		array(
			"value" => "No.9", 
			"label" => "No.9", 
		),
		array(
			"value" => "No.10", 
			"label" => "No.10", 
		),
		array(
			"value" => "No.11", 
			"label" => "No.11", 
		),
		array(
			"value" => "No.12", 
			"label" => "No.12", 
		),
		array(
			"value" => "No.13", 
			"label" => "No.13", 
		),
		array(
			"value" => "No.14", 
			"label" => "No.14", 
		),
		array(
			"value" => "No.15", 
			"label" => "No.15", 
		),
		array(
			"value" => "No.16", 
			"label" => "No.16", 
		),
		array(
			"value" => "No.17", 
			"label" => "No.17", 
		),
		array(
			"value" => "No.18", 
			"label" => "No.18", 
		),
		array(
			"value" => "No.19", 
			"label" => "No.19", 
		),
		array(
			"value" => "No.20", 
			"label" => "No.20", 
		),
		array(
			"value" => "No.21", 
			"label" => "No.21", 
		),
		array(
			"value" => "No.22", 
			"label" => "No.22", 
		),
		array(
			"value" => "No.23", 
			"label" => "No.23", 
		),
		array(
			"value" => "No.24", 
			"label" => "No.24", 
		),
		array(
			"value" => "No.25", 
			"label" => "No.25", 
		),
		array(
			"value" => "No.26", 
			"label" => "No.26", 
		),
		array(
			"value" => "No.27", 
			"label" => "No.27", 
		),
		array(
			"value" => "No.28", 
			"label" => "No.28", 
		),
		array(
			"value" => "No.29", 
			"label" => "No.29", 
		),
		array(
			"value" => "No.30", 
			"label" => "No.30", 
		),
		array(
			"value" => "No.31", 
			"label" => "No.31", 
		),
		array(
			"value" => "No.32", 
			"label" => "No.32", 
		),
		array(
			"value" => "No.33", 
			"label" => "No.33", 
		),
		array(
			"value" => "No.34", 
			"label" => "No.34", 
		),
		array(
			"value" => "No.35", 
			"label" => "No.35", 
		),
		array(
			"value" => "No.36", 
			"label" => "No.36", 
		),);
		
}