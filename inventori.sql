-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 29, 2023 at 02:29 AM
-- Server version: 8.0.34
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventori`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar`
--

CREATE TABLE `barang_keluar` (
  `kode_barang_keluar` int NOT NULL,
  `kode_barang` int NOT NULL,
  `tanggal_barang_keluar` date NOT NULL,
  `stok_keluar` int DEFAULT NULL,
  `penerima` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `barang_keluar`
--

INSERT INTO `barang_keluar` (`kode_barang_keluar`, `kode_barang`, `tanggal_barang_keluar`, `stok_keluar`, `penerima`) VALUES
(9, 4, '2022-12-08', 13, '1'),
(10, 5, '2022-12-08', 34, '1'),
(11, 6, '2022-12-08', 10, '3');

--
-- Triggers `barang_keluar`
--
DELIMITER $$
CREATE TRIGGER `tambahbarangkeluar` AFTER INSERT ON `barang_keluar` FOR EACH ROW BEGIN
UPDATE data_barang SET stok = stok - NEW.stok_keluar
WHERE kode_barang = NEW.kode_barang;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `kode_barang_masuk` int NOT NULL,
  `kode_barang` int NOT NULL,
  `tanggal_barang_masuk` date NOT NULL,
  `stok_masuk` int NOT NULL,
  `penerima` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`kode_barang_masuk`, `kode_barang`, `tanggal_barang_masuk`, `stok_masuk`, `penerima`) VALUES
(6, 1, '2022-12-06', 1, '2'),
(13, 4, '2022-12-08', 10, '1'),
(14, 5, '2022-12-08', 12, '3'),
(15, 6, '2022-12-08', 15, '2');

--
-- Triggers `barang_masuk`
--
DELIMITER $$
CREATE TRIGGER `tambahdatabarang` AFTER INSERT ON `barang_masuk` FOR EACH ROW BEGIN
UPDATE data_barang SET stok = stok + NEW.stok_masuk
WHERE kode_barang = NEW.kode_barang;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `data_barang`
--

CREATE TABLE `data_barang` (
  `kode_barang` int NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `stok` int NOT NULL,
  `merk` varchar(255) NOT NULL,
  `tipe` varchar(255) NOT NULL,
  `serial_number` varchar(255) NOT NULL,
  `kondisi` varchar(255) NOT NULL,
  `lokasi_lemari` varchar(255) NOT NULL,
  `keterangan` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `data_barang`
--

INSERT INTO `data_barang` (`kode_barang`, `nama_barang`, `stok`, `merk`, `tipe`, `serial_number`, `kondisi`, `lokasi_lemari`, `keterangan`) VALUES
(4, 'Hard Disk', 97, 'Vention', '10 AP', '11:22:33:44', 'Baru', 'No.1', 'Hardisk 1 TB Vention'),
(5, 'Router', 178, 'Huawei', '100 HC', '00:99:88:77', 'Baru', 'No.2', 'Rounter Wifi Merk Huawei '),
(6, 'AGP Card', 305, 'Samsung', 'AS10', '11:00:22:99', 'Baru', 'No.3', 'Kabel Data Samsung ');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int NOT NULL,
  `role_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(1, 'Administrator'),
(2, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `permission_id` int NOT NULL,
  `role_id` int NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `action_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`permission_id`, `role_id`, `page_name`, `action_name`) VALUES
(254, 1, 'barang_keluar', 'list'),
(255, 1, 'barang_keluar', 'view'),
(256, 1, 'barang_keluar', 'add'),
(257, 1, 'barang_keluar', 'edit'),
(258, 1, 'barang_keluar', 'editfield'),
(259, 1, 'barang_keluar', 'delete'),
(260, 1, 'barang_keluar', 'import_data'),
(261, 1, 'barang_masuk', 'list'),
(262, 1, 'barang_masuk', 'view'),
(263, 1, 'barang_masuk', 'add'),
(264, 1, 'barang_masuk', 'edit'),
(265, 1, 'barang_masuk', 'editfield'),
(266, 1, 'barang_masuk', 'delete'),
(267, 1, 'barang_masuk', 'import_data'),
(268, 1, 'data_barang', 'list'),
(269, 1, 'data_barang', 'view'),
(270, 1, 'data_barang', 'add'),
(271, 1, 'data_barang', 'edit'),
(272, 1, 'data_barang', 'editfield'),
(273, 1, 'data_barang', 'delete'),
(274, 1, 'data_barang', 'import_data'),
(275, 1, 'role_permissions', 'list'),
(276, 1, 'role_permissions', 'view'),
(277, 1, 'role_permissions', 'add'),
(278, 1, 'role_permissions', 'edit'),
(279, 1, 'role_permissions', 'editfield'),
(280, 1, 'role_permissions', 'delete'),
(281, 1, 'role_permissions', 'import_data'),
(282, 1, 'roles', 'list'),
(283, 1, 'roles', 'view'),
(284, 1, 'roles', 'add'),
(285, 1, 'roles', 'edit'),
(286, 1, 'roles', 'editfield'),
(287, 1, 'roles', 'delete'),
(288, 1, 'roles', 'import_data'),
(289, 1, 'user', 'list'),
(290, 1, 'user', 'view'),
(291, 1, 'user', 'add'),
(292, 1, 'user', 'edit'),
(293, 1, 'user', 'editfield'),
(294, 1, 'user', 'delete'),
(295, 1, 'user', 'import_data'),
(296, 1, 'user', 'userregister'),
(297, 1, 'user', 'accountedit'),
(298, 1, 'user', 'accountview'),
(299, 2, 'barang_keluar', 'list'),
(300, 2, 'barang_keluar', 'view'),
(301, 2, 'barang_keluar', 'add'),
(302, 2, 'barang_keluar', 'edit'),
(303, 2, 'barang_keluar', 'editfield'),
(304, 2, 'barang_keluar', 'delete'),
(305, 2, 'barang_masuk', 'list'),
(306, 2, 'barang_masuk', 'view'),
(307, 2, 'barang_masuk', 'add'),
(308, 2, 'barang_masuk', 'edit'),
(309, 2, 'barang_masuk', 'editfield'),
(310, 2, 'barang_masuk', 'delete'),
(311, 2, 'data_barang', 'list'),
(312, 2, 'data_barang', 'view'),
(313, 2, 'data_barang', 'add'),
(314, 2, 'data_barang', 'edit'),
(315, 2, 'data_barang', 'editfield'),
(316, 2, 'data_barang', 'delete'),
(317, 2, 'user', 'view'),
(318, 2, 'user', 'accountedit'),
(319, 2, 'user', 'accountview');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `login_session_key` varchar(255) DEFAULT NULL,
  `email_status` varchar(255) DEFAULT NULL,
  `password_expire_date` datetime DEFAULT '2023-03-06 00:00:00',
  `password_reset_key` varchar(255) DEFAULT NULL,
  `user_role_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `foto`, `login_session_key`, `email_status`, `password_expire_date`, `password_reset_key`, `user_role_id`) VALUES
(1, 'admin', '$2y$10$knTEvZa5PqSrE0SbbJsP5O/JbIDCMpZGH7duaONJghfBScOnEwaMO', 'admin@gmail.com', 'http://localhost/itsupport/uploads/files/u8fj9heyxvrgcl_.png', NULL, NULL, '2023-03-06 00:00:00', NULL, 1),
(2, 'mivanfadilah', '$2y$10$sQPE93Ntn8UCFTB/AljQLOh0.W6AXHY2A/gpoP/2JY80GF2gFr0fy', 'muhammadivanfadilah@gmail.com', 'http://localhost/itsupport/uploads/files/6lc73fd1bz9xq4r.JPG', '63ce4fa09ba66e877791ca1d50f6cdc3', NULL, '2023-03-06 00:00:00', NULL, 1),
(3, 'ivan', '$2y$10$myYWyOy21K/Aqpy6Az6voOWYGSckOZe6l2DsHtsoG2XgBNlNpMHji', 'ivan@gmail.com', 'http://localhost/itsupport/uploads/files/7og0hjd912ts8yc.jpg', NULL, NULL, '2023-03-06 00:00:00', NULL, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`kode_barang_keluar`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`kode_barang_masuk`);

--
-- Indexes for table `data_barang`
--
ALTER TABLE `data_barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`),
  ADD UNIQUE KEY `role_name` (`role_name`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  MODIFY `kode_barang_keluar` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `kode_barang_masuk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `data_barang`
--
ALTER TABLE `data_barang`
  MODIFY `kode_barang` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `permission_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=320;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
